

    Steps to setup calabash-android on Ubuntu.
    1. Install Oracle JDK 7 and add it to your path.
    2. Install Ruby 1.9.3 using rvm

        gem install calabash-android



```bash
bundle exec calabash-android run apk/todolist.apk -p android 
or try in command line using "sudo calabash-android run apk/todolist.apk -p android
"
```
