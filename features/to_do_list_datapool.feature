Feature: End to end to do list using data pool

  @android
  Scenario Outline: As a user I can add/delete to do list
    When I press the "Add To Do List" button
    Then I see the text "Add New List"
    When I enter text "<ttitle>" into field with id "titleEdittext"
    And I enter text "<dttitle>" into field with id "detailEdittext"
    And I press the "SAVE" button
    Then I see the text "<ttitle>"
    And I see the text "<dttitle>"
    When I press view with id "trash_iv"
    And I press the "YES" button
    Then I see the text "Add To Do List"

  Examples:
   |ttitle|dttitle|
   |DOP|DoctorAppointment|
   |Visa|Visa Appointment|
   |NorthIndiaTour|Travel Contact|
